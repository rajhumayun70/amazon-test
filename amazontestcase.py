import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from amazoncaptcha import AmazonCaptcha
from selenium.webdriver.chrome.options import Options
import chromedriver_autoinstaller
import time

@pytest.fixture
def browser():
    # Install chromedriver if not found and get path
    chromedriver_autoinstaller.install()

    # Create a new instance of Chrome options
    chrome_options = Options()

    # Initialize the Chrome driver with the configured options
    driver = webdriver.Chrome(options=chrome_options)
    yield driver


def test_search_product(browser):
    # URL of the Amazon website
    url = "https://www.amazon.com/"

    # Open the Amazon website
    browser.get(url)

    # Solve captcha if present
    try:
        captcha_link = browser.find_element(By.XPATH, "//div[@class = 'a-row a-text-center']//img").get_attribute('src')
        captcha = AmazonCaptcha.fromlink(captcha_link)
        captcha_value = AmazonCaptcha.solve(captcha)
        input_field = browser.find_element(By.ID, "captchacharacters")
        input_field.send_keys(captcha_value)
        button = browser.find_element(By.CLASS_NAME, "a-button-text")
        button.click()
    except Exception as e:
        print("No captcha found:", e)

    time.sleep(2)

    # search input field and enter the search text
    input_search = browser.find_element(By.ID, "twotabsearchtextbox")
    input_search.send_keys('long boards skateboard')
    time.sleep(2)

    # click the search button
    search_btn = browser.find_element(By.ID, "nav-search-submit-button")
    search_btn.click()

    # Wait for the search results to load
    time.sleep(5)

    # Verify if the search results contain the expected product title
    expected_title = "Retrospec Zed Longboard Skateboard Complete Cruiser | Bamboo & Canadian Maple Wood Cruiser w/ Reverse Kingpin Trucks for Commuting, Cruising, Carving & Downhill Riding"
    try:
        product_title = browser.find_element(By.XPATH, "//*[@id='search']/div[1]/div[1]/div/span[1]/div[1]/div[3]/div/div/span/div/div/div[3]/div[2]/h2/a/span").text
        assert expected_title in product_title
        print("The specific product is found in the search results.")
    except Exception as e:
        print("Error:", e)
        print("The specific product is not found in the search results.")

